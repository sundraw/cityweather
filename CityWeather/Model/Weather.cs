﻿using System;
using Newtonsoft.Json;

namespace CityWeather
{
    public class Weather
    {
        [JsonProperty("temp")]
        public double Temperature { get; set; }

        [JsonProperty("temp_min")]
        public double TemperatureMin { get; set; }

        [JsonProperty("temp_max")]
        public double TemperatureMax { get; set; }

        public double Humidity { get; set; }
        public double Pressure { get; set; }
    }
}
