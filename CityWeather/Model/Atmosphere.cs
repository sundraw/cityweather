﻿using System;
using Newtonsoft.Json;

namespace CityWeather
{
    public class Atmosphere
    {
        [JsonProperty("main")]
        public string Condition { get; set; }
        public string Description { get; set; }
    }
}
