﻿using System;
using Newtonsoft.Json;

namespace CityWeather
{
    public class Location
    {
        [JsonProperty("lat")]
        public double Latitude { get; set; }
        [JsonProperty("lon")]
        public double Longitude { get; set; }
    }
}
