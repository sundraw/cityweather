﻿using System;
using Newtonsoft.Json;

namespace CityWeather
{
    public class Wind
    {
        public double Speed { get; set; }

        [JsonProperty("deg")]
        public double Direction { get; set; }
    }
}
