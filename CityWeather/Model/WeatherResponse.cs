﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace CityWeather
{
    public class WeatherResponse
    {
        public string Name { get; set; }

        [JsonProperty("coord")]
        public Location Location { get; set; }

        [JsonProperty("weather")]
        public List<Atmosphere> Conditions { get; set; }

        [JsonProperty("main")]
        public Weather Weather { get; set; }

        public Wind Wind { get; set; }
    }
}
