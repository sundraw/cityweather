﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CityWeather
{
    public class WeatherManager
    {
        const string BaseUrl = "http://api.openweathermap.org/data/2.5/weather?q={0}&APPID={1}&units=metric";
        const string ApiKey = "ea1ef1c522b0d186f393266cbb95f803";

        static readonly WeatherManager instance = new WeatherManager();

        WeatherManager() {}

        public static WeatherManager Instance
        {
            get 
            {
                return instance;
            }
        }

        public WeatherResponse Weather { get; private set; }

        public async Task<bool> RequestWeather(string city)
        {
            using (var client = new HttpClient())
            {
                var url = string.Format(BaseUrl, city, ApiKey);
                var json = await client.GetStringAsync(url);
                Debug.WriteLine("Response: {0}", json);

                WeatherResponse response = JsonConvert.DeserializeObject<WeatherResponse>(json);

                if (response != null)
                {
                    Weather = response;

                    return true;
                }
            }

            return false;
        }
    }
}
