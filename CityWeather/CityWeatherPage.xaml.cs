﻿using System;
using Plugin.Connectivity;
using Plugin.Settings;
using Xamarin.Forms;

namespace CityWeather
{
    public partial class CityWeatherPage : ContentPage
    {
        const string SaveCityKey = "save city";
        const string CityNameKey = "city name";

        public bool SavingEnabled
        {
            get
            {
                return CrossSettings.Current.GetValueOrDefault(SaveCityKey, false);
            }

            set
            {
                CrossSettings.Current.AddOrUpdateValue(SaveCityKey, value);
            }
        }

        public CityWeatherPage()
        {
            InitializeComponent();

            NavigationPage.SetBackButtonTitle(this, "Back");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (SavingEnabled)
            {
                txtCity.Text = CrossSettings.Current.GetValueOrDefault(CityNameKey, string.Empty);
            }
        }

        async void OnSubmit(object sender, EventArgs args)
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                await DisplayAlert("No network", "There is no Internet connection at the moment", "OK");
                return;
            }

            var city = txtCity.Text;
            if (string.IsNullOrWhiteSpace(city))
            {
                await DisplayAlert("Which city?", "Have you forgotten to specify the city?", "OK");
                return;
            }

            activityIndicator.IsRunning = true;

            CrossSettings.Current.AddOrUpdateValue(CityNameKey, city);

            var success = await WeatherManager.Instance.RequestWeather(city);
            activityIndicator.IsRunning = false;
            if (!success)
            {
                await DisplayAlert("A Problem", string.Format("Couldn't retrieve weather for {0}", txtCity.Text), "OK");
                return;
            }

            await Navigation.PushAsync(new WeatherDetailsPage());
        }
    }
}
