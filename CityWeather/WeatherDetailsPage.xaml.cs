﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CityWeather
{
    public partial class WeatherDetailsPage : ContentPage
    {
        public WeatherDetailsPage()
        {
            InitializeComponent();

            BindingContext = WeatherManager.Instance.Weather;
        }
    }
}
